// 2. Get all items containing only Vitamin C.
const data = require("./data/3-arrays-vitamins.cjs");
/// forEach is better. beacause he we need to only iterate and check the Vitamin c and pushes to the output
function q1(data) {
  let vitaminC = [];
  data.forEach((index) => {
    let word = (index.contains).toLowerCase()
    if( word.includes('vitamin c') ){  // checking whether the it contains word vitamin c. if exits then pushes.
        vitaminC.push(index.name);
    }
  });
  return vitaminC;
}
console.log(q1(data));
