// 3. Get all items containing Vitamin A.
const data = require("./data/3-arrays-vitamins.cjs");
/// forEach is better. beacause he we need to only iterate and check the Vitamin c and pushes to the output
function q3(data) {
  let vitaminA = [];
  data.forEach((index) => {
    let word = (index.contains).toLowerCase()
    if( word.includes('vitamin a') ){  // checking whether the it contains word vitamin a. if exits then pushes.
        vitaminA.push(index.name);
    }
  });
  return vitaminA;
}
console.log(q3(data));
