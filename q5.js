// 5. Sort items based on number of Vitamins they contain.

// NOTE: Do not change the name of this file


const data = require("./data/3-arrays-vitamins.cjs");


function countVitamins( aVitamin , bVitamin ){
    // the countVitamine function . if we have more vitamins. then it is in the format of ( type1 , typ2 ). it is is seperated by comma.
    // Splitting them we get to know the number of vitamins.

    let length1 = ( ( aVitamin.contains ).split(',')).length
    let length2 = ( ( bVitamin.contains ).split(',')).length
    return [length1 , length2]
}

function q5(){
    const sortedItems = data.sort((a, b) => {
        let dataValues = countVitamins(a,b)
        // return (dataValues[0] < dataValues[1])  ? 1 : (dataValues[0]>dataValues[1]) ? -1 : 0  // thsi is for  descending order
        return (dataValues[0] > dataValues[1])  ? 1 : (dataValues[0]<dataValues[1]) ? -1 : 0  // this is for ascending order
    });
    return sortedItems
}

console.log(q5())