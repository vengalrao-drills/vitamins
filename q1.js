// 1. Get all items that are available

const data = require("./data/3-arrays-vitamins.cjs");
/// forEach is better. beacause he we need to only iterate and give the name - to the output
function q1(data) {
  let allVitamins = [];
  data.forEach((index) => {
    allVitamins.push(index.name); // iterating and pushing all the vitamin names to array
  });
  return allVitamins; 
}
console.log(q1(data));

 
