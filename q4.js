// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

// and so on for all items and all Vitamins.

const data = require("./data/3-arrays-vitamins.cjs");
 

function q4(data) {
  let infoVitamin = {};
  data.forEach((object) => {
    let word = object.contains;
    let wordArray = word.split(","); 
    wordArray.forEach((index) => {
      index = index.trim()
      if (infoVitamin[index] == undefined) {
        // checking whether  Vitamin  is present in object or not. if not present.
        infoVitamin[index] = [object["name"]]; // then will make a array attacted to it. key is vitamin. values is in the form of array
      } else {
        infoVitamin[index].push(object["name"]); // if present before. then it is pushed to the corresponding vitamin.
      }
    });
  });
  return infoVitamin; // returing the vitamin data
}

console.log(q4(data)); 
